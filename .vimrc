au BufRead,BufNewFile *.md set filetype=markdown

set ruler
set smartindent
set smartcase
set tabstop=4
set shiftwidth=4
set expandtab
set incsearch
set hlsearch

set colorcolumn=100
syntax on
set number
highlight ColorColumn ctermbg=7

set hidden
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>

noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>
